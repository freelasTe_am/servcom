<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package servcom
 */

get_header();
?>
<?php $post_id = 7 ?>
<main id="page-home" class="page" key="home">

	<section id="db01">
						<img src="<?php echo get_template_directory_uri(); ?>/img/conheca.png" class="con">

		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="text-db">
						<?php the_field('texto_do_banner' , $post_id); ?>
						<div class="aps">
							<?php

							if(get_field('imagem_app' , $post_id)): ?>

								<?php while(the_repeater_field('imagem_app' , $post_id)): ?>
									<img src="<?php the_sub_field('img_app'); ?>" alt="<?php the_sub_field('alt'); ?>" />
								<?php endwhile; ?>

							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="phone">
						<img src="<?php the_field('imagem_do_banner', $post_id) ?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="db02">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12">
					<?= the_field('texto_inicial', $post_id); ?>
				</div>			
				<div class="col-md-12">
					
					<div class="section-hero">

						<div class="slide">
							<div class="block_left-slider"></div>
							<div class="slide-itens">
								<?php
								if(get_field('conteudo_slider' , $post_id)):  $x = -1; ?>
									<?php while(the_repeater_field('conteudo_slider' , $post_id)): $x++ < 2; ?>

										<div class="<?php echo $x ?> list_item <?= "item".($x === 0 ? " active" : ""); ?> "  data-index="<?= $x; ?>">
											<div class="col-md-8">
												
												<div class="item icon i_<?php echo $x ?>" data-index="<?= $x ?>">
													<button>
														<img src="<?= the_sub_field('icone') ?>" alt="">
													</button>
													<span class="number num_<?php echo $x ?>">0<?= $x + 1 ?></span>
												</div>
												<div class="text_slide t_<?php echo $x ?>">
													<div class="itens" data-index="<?= $x ?>" >
														
													</div>
													<?= the_sub_field('texto_lateral'); ?>
												</div>
											</div>
											<div class="device <?php echo $x ?> <?= "item".($x === 0 ? " active" : ""); ?> " data-index="<?= $x ?>">

												<img class="item" data-index="<?= $x ?>" src="<?php the_sub_field('imagem') ?>" alt="">

											</div>
										</div>
										
									<?php endwhile; ?>
								<?php endif; ?>

							</div>
							<div class="slide-arrows">
								<a href="javascript:;" class="arrow-left arrow" data-direction="prev">
									<img src="<?php echo get_template_directory_uri(); ?>/img/seta_esquerda.png" class="">
									
								</a>
								<a href="javascript:;" class="arrow-right arrow" data-direction="next">
									<img src="<?php echo get_template_directory_uri(); ?>/img/seta_direita.png" class="">
									
								</a>

								<div class="dots">
									<?php $x = -1; while ($x++ < 2):    ?>
									<div class="item" data-index="<?= $x ?>" >
										<button><?= $x + 1 ?></button>
									</div>
								<?php endwhile; ?>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="db03">
		<div class="container">
			<div class="vid">
				<div class="row">
					<div class="col-md-10 offset-md-1 offset-sm-0">
						<h2>ASSISTA A DEMONSTRAÇÃO POR MEIO DO VÍDEO </h2>
						<?php the_field('video' , $post_id) ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>
<?php
get_footer();
