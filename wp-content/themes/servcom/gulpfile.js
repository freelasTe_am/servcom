const gulp          = require("gulp");
const autoprefixer  = require("gulp-autoprefixer");
const cleancss      = require("gulp-clean-css");
const rename        = require("gulp-rename");
const sass          = require("gulp-sass");
const sourcemaps    = require("gulp-sourcemaps");
const broswerSync   = require("browser-sync").create();
const watch         = require("gulp-watch");
const notifier      = require('node-notifier');

/** @task: Compile. */
gulp.task("compile", function() {
  gulp.src("./sass/main.scss")
    .pipe(sourcemaps.init())
      .pipe(sass().on("error", err => {
        notifier.notify("Compiler error. Check the terminal.");
        console.log("|=")
        console.log(err.formatted)
        console.log("|=")
      }))
      .pipe(autoprefixer({browsers: ["last 2 versions"], cascade: false}))
      .pipe(cleancss())
      .pipe(rename("main.min.css"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./dist/"))
    .pipe(broswerSync.reload({stream: true}));
});

/** @watch */
gulp.task("watch", function() {
  broswerSync.init({
    proxy: "http://localhost/servcom",
    host: "localhost/servcom"
  });

  /** @compile */
  watch("./sass/**/*.scss", () => gulp.start("compile"));

  /** @reload */
  watch("../**/*.php", broswerSync.reload);
  watch("./js/**/*.js", broswerSync.reload);
});

/** @default */
gulp.task("default", ["watch"]);
