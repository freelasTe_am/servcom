<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package servcom
 */

?>

</div><!-- #content -->
<?php $post_f = 32 ?>
<footer id="colophon" class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="quem">
					<h2>_Quem somos</h2>
					<?php the_field('texto_quem_somos' , $post_f); ?>
					<?php

					if(get_field('logos_rodape' , $post_f)): ?>

						<?php while(the_repeater_field('logos_rodape' , $post_f)): ?>
							<img src="<?php the_sub_field('imagem_rodape'); ?>" alt="<?php the_sub_field('alt'); ?>" />
						<?php endwhile; ?>

					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-6 offset-md-2 offset-sm-0">
				<h2>_Fale conosco</h2>
				<?php the_field('texto_fale_conosco' , $post_f); ?>
				<?php echo do_shortcode('[contact-form-7 id="54" title="Contato"]'); ?>
				<div class="clearfix"></div>
				<div class="local">
					<div class="local_">
						<?php the_field('localizacao' , $post_f); ?>
					</div>
					<div class="cont">
						<?php the_field('contatos' , $post_f); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer><!-- #colophon -->
<div class="sub_f">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="app_footer">
					Baixe nosso aplicativo
					<?php

					if(get_field('logos_rodape_' , $post_f)): ?>

						<?php while(the_repeater_field('logos_rodape_' , $post_f)): ?>
							<img src="<?php the_sub_field('imagem_rodape'); ?>" alt="<?php the_sub_field('alt'); ?>" />
						<?php endwhile; ?>

					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-4">
				<a href="mailto:dcomercial@servcom.com.br">dcomercial@servcom.com.br</a>
			</div>
		</div>
	</div>
	
</div>
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript">
	const p = {
		home : $('#page-home')
	}
	function setup__heroSlide() {


		/** Get main elements. */
		let section = $('.section-hero');
		let nav = section.find(".slide .slide-arrows");
		let number = section.find(".itens");
		let icon = section.find(".icon");

		/** Activate the first navigation item. */
		nav.find('.item[data-index="0"]').addClass("active");

		// /** @setInterval */
		// window.heroInterval = setInterval(() => hero__changeSlide("next"), 7000);

		/** Bind click event to the arrows. */
		nav.find(".arrow").on("click", function() {
			hero__changeSlide($(this).data("direction"));
		})

		/** Bind click event to the thumbnails. */
		nav.find(".dots .item button").on("click", function() {
			let slideIndex = $(this).parent(".item").data("index");
			hero__changeSlide(slideIndex);
		});
		// number.find("button").on("click", function() {
		// 	console.log('teste')
		// 	let slideIndex = $(this).parent(".itens").data("index");
		// 	hero__changeSlide(slideIndex);
		// });
		icon.find("button").on("click", function() {
			console.log('teste')
			let slideIndex = $(this).parent(".icon").data("index");
			hero__changeSlide(slideIndex);
		});
	}

	if(p.home.length !== 0) setup__heroSlide();

	/** @action: Change slide. */
	function hero__changeSlide(direction) {

		let isSpecific = !(direction === "next" || direction === "prev");

		/** Get main elements. */
		let section = $('.section-hero');
		let slide = section.find(".slide");

		if (slide.is(".changing")) return;
		slide.addClass("changing");

		/** @clearInterval */
		clearInterval(window.heroInterval);

		/** Get the current slide. */
		let currentSlide = slide.find(".item.active");
		let currentSlideIndex = currentSlide.data("index");

		/** Get the next slide. */
		let nextSlideIndex = (direction === "next") ? currentSlideIndex + 1 : currentSlideIndex - 1;
		if (nextSlideIndex < 0) nextSlideIndex = (slide.find(".slide-itens .item").length - 1);
		if (nextSlideIndex > 2) nextSlideIndex = (slide.find(".slide-itens .item").length - 0);
		if (nextSlideIndex === slide.find(".slide-itens .item").length) nextSlideIndex = 0;
		if (isSpecific) nextSlideIndex = direction;
		let nextSlide = slide.find(`.item[data-index="${nextSlideIndex}"]`);

		/** Change the active slide. */
		currentSlide.removeClass("active");

		nextSlide.addClass("active")
		slide.removeClass("changing");

	}
	$('a[href^="#"]:not([data-toggle])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      let target = $(this.hash);
      
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top -70
        }, 1000);
        return false;
      }
    }
  });

	$(window).scroll(function() {
    let scroll = $(window).scrollTop();

    if (scroll >= 500) {
        $('header').addClass("menu_fix");
        $('body').addClass("bd_p");
    } else {
        $('header').removeClass("menu_fix");
        $('body').removeClass("bd_p");

    }
});
</script>
</body>
</html>
