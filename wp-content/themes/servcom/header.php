<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package servcom
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(). '/dist/main.min.css' ?>">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<?php $post_f = 32 ?>
		<header id="masthead" class="site-header">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<?php 
						if(get_field('logos_rodape' , $post_f)): ?>

							<?php while(the_repeater_field('logos_rodape' , $post_f)): ?>
								<img class="hidden_fixed" src="<?php the_sub_field('imagem_rodape'); ?>" alt="<?php the_sub_field('alt'); ?>" />
							<?php endwhile; ?>

						<?php endif; ?>
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo_fixed.png" class="s_fixed">

					</div>
					<div class="col-md-6">
						<div id="menu">
							<ul>
								<li>
									<a href="#db02" title="O APP">O APP</a>
								</li>
								<li>
									<a href="#db03" title="Demonstração">Demonstração</a>
								</li>
								<li class="fale">
									<a href="#colophon" title="Fale conosco">Fale conosco</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>


		</header><!-- #masthead -->

		<div id="content" class="site-content">
