
/**
 * Setup scroll to top action.
 */
$('[rel="scroll-to-top"]').on("click", () => $("body, html").animate({scrollTop: 0}, 3000, $.bez([0.5, 0, 0, 1])))

/**
 * Anchor links.
 */
$('[rel="anchor"]').on("click", function() {

  /** Get anchor information. */
  let section_key = $(this).attr("anchor-to");
  let section = $('[anchor-key="'+section_key+'"]');
  let offset = section.offset().top;

  /** Scroll window to section. */
  $("html, body").animate({scrollTop: offset - 120}, 3000, $.bez([0.5, 0, 0, 1]));
})

/**
 * Displays a progress screen.
 *
 * @param {string} message Message to be displayed while the progress is still going.
 */
function progress_screen(message) {

  /** @setup [Screen] */
  let screen_html = ''+
    '<div class="screen-progress">'+
      '<h2 class="screen-message">'+message+'</h2>'+
      '<div class="spinner">'+
        '<span></span><span></span>'+
      '</div>'+
    '</div>';
  let screen = $(screen_html);

  /** Add shadow to main body. */
  $("body").addClass("shadow");

  /** Append screen to main body. */
  $("body").append(screen);

  /** Display screen. */
  setTimeout(() => screen.addClass("open"), 300);

  /** Bind event to close. */
  screen.on("close", function(e, message) {
    if (message) {
      screen.find(".screen-message").text(message);
      setTimeout(() => {
        screen.removeClass("open");
        setTimeout(() => {
          screen.remove();
          $("body").removeClass("shadow");
        }, 1000);
      }, 1000);
    } else {
      screen.removeClass("open");
      setTimeout(() => {
        screen.remove();
        $("body").removeClass("shadow");
      }, 1000);
    }
  });

  /** @return */
  return screen;
}

/**
 * Makes elements appear as the user scrolls down.
 */
function appear() {

  /** Get main variables. */
  let vh = $(window).height();
  let scroll = window.scrollY;

  /** @setup */
  $("[appear]").each(function() {
    let el = $(this);

    /** Get element position in which the event should be triggered. */
    let on = el.attr("appear");
    if (on.indexOf("vh") != -1) {
      on = (vh / (100 / parseInt(on.replace("vh", ""))));
    } else {
      on = Number(on);
    }

    /** Get additional options. */
    let delay = (el.attr("appear-delay")) ? Number(el.attr("appear-delay")) : 0;
    let delay_target = (el.attr("appear-target-delay")) ? Number(el.attr("appear-target-delay")) : 0;
    let done = (el.attr("appear-done")) ? Number(el.attr("appear-done")) : false;

    /** Get offset from top. */
    let offset = el.offset().top;

    // TARGET (?)
    // ----------------------------------------

    /** Check for a target key. */
    let target_key = el.attr("appear-target");
    if (target_key) {
      if ((scroll + vh) > (offset + on)) {
        let targets = el.find('[appear-key="'+target_key+'"]');

        /** Loop through each target. */
        setTimeout(() => {
          targets.each(function(index, item) {

            /** Get item information. */
            item = $(item);
            let this_delay = index * delay_target;

            /** Trigger appear. */
            setTimeout(() => {
              $(item).addClass("appear");
              $(item).trigger("appear");

              setTimeout(() => $(item).addClass("done"), 10);
            }, this_delay);
          })
        }, delay)
      }
      return;
    }

    // TRIGGER
    // ----------------------------------------

    /** Add class when the event is triggered. */
    setTimeout(function() {
      if ((scroll + vh) > (offset + on)) el.trigger("appear").addClass("appear");

      /** Done. */
      if (done) setTimeout(() => el.addClass("appear-done"), done);
    }, delay);
  })
}
$(window).on("entered", () => appear());
$(window).on("scroll", () => appear());

// WORDPRESS
// ----------------------------------------

/**
 * Uploads a file to the server.
 *
 * @param  {Object}  file File to be uploaded.
 * @return {Promise}      Resolved with the file URL.
 */
function ajax_upload_file(file) {
  return new Promise(resolve => {

    /** @setup [Request] */
    let data = new FormData();
    data.append("action", "upload_file");
    data.append("file", file.info);

    /** @request */
    $.ajax({
      url: window.ajaxurl,
      method: "POST",
      data: data,
      cache: false,
      contentType: false,
      processData: false,
      dataType: "json",
      success: response => {
        if (response.status === "error") {
          resolve(false);
        } else {
          resolve(response.file_url);
        }
      },
      error: error => resolve(false)
    })
  })
}

// LOCATION
// ----------------------------------------

/**
 * Get user's location from their IP address.
 *
 * @param {Function} callback Callback for when it's finished.
 */
function get_user_location(callback) {

  /** @request */
  $.ajax({
    url: "http://ip-api.com/json",
    method: "GET",
    success: response => {
      if (response.status === "success") {
        callback(response);
      } else {
        callback(false);
      }
    },
    error: error => callback(false)
  })
}
