
/**
 * Setup Google Maps for all elements.
 */
$('[rel="google_maps"]').each(function() {

  /** Gather main information. */
  let map_el = this;
  let map_coords;

  /** Gather coordinates. */
  let raw_coords = $(this).data('coords').split(',');

  /* @setup [Options] */
  let map_options = {
    zoom: 14,
    center: {
      lat: parseFloat(raw_coords[0]),
      lng: parseFloat(raw_coords[1])
    }
  }

  /** Generate map. */
  let map = new google.maps.Map(map_el, map_options);

  /** Add marker. */
  let marker = new google.maps.Marker({
    position: {
      lat: parseFloat(raw_coords[0]),
      lng: parseFloat(raw_coords[1])
    },
    map: map
  })
})
