// ---------------------------------------------
// Main
// ---------------------------------------------


// --> Page variable
const p_emp = $('[key="empreendimento-interna"]');
const p_emp_f = {};

// ---------------------------------------------
// Section hero
// ---------------------------------------------

// --> On load page
$(document).ready(function(){
  setTimeout(function(){
    p_emp.find('.section-hero').addClass('animate');
  }, 1000);
});

$(window).on('scroll', () => {

  /** Check current offset. */
  offset = $(window).scrollTop();
  if (offset > 550) {

    $('.look').addClass('mobi_f');
  } else {
    $('.look').removeClass('mobi_f');;

  }
});

$('[rel="menu-toggler"]').on("click", () => {
  $("#main-menu").addClass("open");
  $("body").css("overflow", "hidden");
});
$('#main-menu [rel="close"]').on("click", () => {
  $("#main-menu").removeClass("open");
  $("body").removeAttr("style");
});

// ---------------------------------------------
// Section diferenciais
// ---------------------------------------------

// --> Slider diferenciais
p_emp.find('.section-diferenciais .slider').owlCarousel({
  loop: true,
  nav: false,
  items: 1,
  autoplay:true,
  autoplayTimeout: 6000,
  smartSpeed: 650,
});

// --> Arrows slider diferenciais
p_emp.find('.section-diferenciais .arrows .arrow').on('click', function(){

  // |||> Variables
  var owl = p_emp.find('.section-diferenciais .slider');

  // |||> Conditionals for slider next and previous
  if ($(this).hasClass('arrow-next')) {
    owl.trigger('next.owl.carousel');
  } else {
    owl.trigger('prev.owl.carousel');
  }
});

// --> Get width owl dots
p_emp.find('.section-diferenciais .owl-dots').each(function(){

  // |> Variables
  let width = $(this).outerWidth();
  let result = width + 100;

  // |> Set to arrows
  p_emp.find('.section-diferenciais .arrows').css('width', result + 'px');
});

// ---------------------------------------------
// Section porque escolher
// ---------------------------------------------

// --> Slider porque escolher
p_emp.find('.section-porque-escolher .slider').owlCarousel({
  loop: true,
  nav: false,
  items: 4,
  autoplay:true,
  autoplayTimeout: 6000,
  smartSpeed: 650,
});

// --> Arrows slider porque escolher
p_emp.find('.section-porque-escolher .arrows .arrow').on('click', function(){

  // |||> Variables
  var owl = p_emp.find('.section-porque-escolher .slider');

  // |||> Conditionals for slider next and previous
  if ($(this).hasClass('arrow-next')) {
    owl.trigger('next.owl.carousel');
  } else {
    owl.trigger('prev.owl.carousel');
  }
});

// ---------------------------------------------
// Section fotos
// ---------------------------------------------

// --> Slider fotos
p_emp.find('.section-fotos .slider').owlCarousel({
  loop: false,
  nav: false,
  items: 1,
  autoplay:true,
  autoplayTimeout: 6000,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  smartSpeed: 650,
});

// --> Arrows slider fotos
p_emp.find('.section-fotos .arrows .arrow').on('click', function(){

  // |||> Variables
  var owl = p_emp.find('.section-fotos .slider');

  // |||> Conditionals for slider next and previous
  if ($(this).hasClass('arrow-next')) {
    owl.trigger('next.owl.carousel');
  } else {
    owl.trigger('prev.owl.carousel');
  }
});

// --> Get width owl dots
p_emp.find('.section-fotos .owl-dots').each(function(){

  // |> Variables
  let width = $(this).outerWidth();
  let result = width + 100;

  // |> Set to arrows
  p_emp.find('.section-fotos .arrows').css('width', result + 'px');
});

// --> Slider navs on click
p_emp.find('.section-fotos .navigation ul li').on('click', function() {

  // |> Variables
  var index = $(this).data('ii');

  // |> Change active item
  $(this).parent().find('li').removeClass('active');
  $(this).addClass('active');

  // |> Go to gallery
  if (index != 'all') {
    p_emp.find('.section-fotos .slider').trigger("to.owl.carousel", [index, 1, true]);
  } else {
    p_emp.find('.section-fotos .slider').trigger("to.owl.carousel", [0, 1, true]);
  }
}); 

// --> Slider after change
p_emp.find('.section-fotos .slider').on('changed.owl.carousel', function(event) {

  // ||> Variables
  var current = event.item.index,
  ai      = p_emp.find('.section-fotos .navigation ul li.active').data('ii'),
  gi      = p_emp.find('.section-fotos .navigation ul li[data-ii="' + current + '"]');

  // ||> Fullscreen
  var fs = p_emp.find('.section-fotos .fullscreen a');
  fs.attr('href', '#gallery_fotos-' + (current + 1));

  // ||> Functin to slider active nav
  if (ai != 'all' && gi.length != 0) {
    p_emp.find('.section-fotos .navigation ul li').removeClass('active');
    p_emp.find('.section-fotos .navigation ul li[data-ii="' + current + '"]').addClass('active');
  }
}); 

// ---------------------------------------------
// Section plantas
// ---------------------------------------------

// --> Slider plantas
p_emp.find('.section-plantas .slider').owlCarousel({
  loop: false,
  nav: false,
  items: 1,
  autoplay:true,
  autoplayTimeout: 6000,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  smartSpeed: 650,
});

// --> Arrows slider plantas
p_emp.find('.section-plantas .arrows .arrow').on('click', function(){

  // |||> Variables
  var owl = p_emp.find('.section-plantas .slider');

  // |||> Conditionals for slider next and previous
  if ($(this).hasClass('arrow-next')) {
    owl.trigger('next.owl.carousel');
  } else {
    owl.trigger('prev.owl.carousel');
  }
});

// --> Get width owl dots
p_emp.find('.section-plantas .owl-dots').each(function(){

  // |> Variables
  let width = $(this).outerWidth();
  let result = width + 100;

  // |> Set to arrows
  p_emp.find('.section-plantas .arrows').css('width', result + 'px');
});

// --> Slider navs on click
p_emp.find('.section-plantas .navigation ul li').on('click', function() {

  // |> Variables
  var index = $(this).data('ii');

  // |> Change active item
  $(this).parent().find('li').removeClass('active');
  $(this).addClass('active');

  // |> Go to gallery
  if (index != 'all') {
    p_emp.find('.section-plantas .slider').trigger("to.owl.carousel", [index, 1, true]);
  } else {
    p_emp.find('.section-plantas .slider').trigger("to.owl.carousel", [0, 1, true]);
  }
}); 

// --> Slider after change
p_emp.find('.section-plantas .slider').on('changed.owl.carousel', function(event) {

  // ||> Variables
  var current = event.item.index,
  ai      = p_emp.find('.section-plantas .navigation ul li.active').data('ii'),
  gi      = p_emp.find('.section-plantas .navigation ul li[data-ii="' + current + '"]');

  // ||> Fullscreen
  var fs = p_emp.find('.section-plantas .fullscreen a');
  fs.attr('href', '#gallery_plantas-' + (current + 1));

  // ||> Functin to slider active nav
  if (ai != 'all' && gi.length != 0) {
    p_emp.find('.section-plantas .navigation ul li').removeClass('active');
    p_emp.find('.section-plantas .navigation ul li[data-ii="' + current + '"]').addClass('active');
  }
}); 




// ---------------------------------------------
// Section video
// ---------------------------------------------

// --> Slider plantas
p_emp.find('.section-video .slider').owlCarousel({
  loop: false,
  nav: false,
  items: 1,
  autoplay:true,
  autoplayTimeout: 6000,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  smartSpeed: 650,
});

// --> Arrows slider plantas
p_emp.find('.section-video .arrows .arrow').on('click', function(){

  // |||> Variables
  var owl = p_emp.find('.section-video .slider');

  // |||> Conditionals for slider next and previous
  if ($(this).hasClass('arrow-next')) {
    owl.trigger('next.owl.carousel');
  } else {
    owl.trigger('prev.owl.carousel');
  }
});

// --> Get width owl dots
p_emp.find('.section-video .owl-dots').each(function(){

  // |> Variables
  let width = $(this).outerWidth();
  let result = width + 100;


});




// ---------------------------------------------
// Section evolucao
// ---------------------------------------------

// --> Slider evolucao
p_emp.find('.section-evolucao .slider').owlCarousel({
  loop: false,
  nav: false,
  items: 1,
  autoplay:true,
  autoplayTimeout: 6000,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  smartSpeed: 650,
});

// --> Arrows slider evolucao
p_emp.find('.section-evolucao .arrows .arrow').on('click', function(){

  // |||> Variables
  var owl = p_emp.find('.section-evolucao .slider');

  // |||> Conditionals for slider next and previous
  if ($(this).hasClass('arrow-next')) {
    owl.trigger('next.owl.carousel');
  } else {
    owl.trigger('prev.owl.carousel');
  }
});

// --> Get width owl dots
p_emp.find('.section-evolucao .owl-dots').each(function(){

  // |> Variables
  let width = $(this).outerWidth();
  let result = width + 100;

  // |> Set to arrows
  p_emp.find('.section-evolucao .arrows').css('width', result + 'px');
});

// --> Slider after change
p_emp.find('.section-evolucao .slider').on('changed.owl.carousel', function(event) {

  // ||> Variables
  var current = event.item.index;

  // ||> Fullscreen
  var fs = p_emp.find('.section-evolucao .fullscreen a');
  fs.attr('href', '#gallery_evolucao-' + (current + 1));
}); 

// --> Steps svg 
p_emp.find('.section-evolucao .step').on('appear', function(){
  if ($(this).hasClass('done')) return;

  // |> Variables
  let $this       = $(this);
  let attr      = $(this).find('.count').attr('data-count');
  let stroke      = '376.8';
  let count       = attr / 10;
  let count_result  = count * stroke / 10 - stroke;
  let str       = ''+count_result+'';
  let result      = str.replace("-", "");

  // |> Setup count
  $this.prop('Count', 0).animate({
    Count: attr
  },{
    duration: 1000,
    step: function(val) {
      $this.find('.count').html(val.toFixed(0) + '%');
    }
  })

  // |> Set stroke result
  $(this).find('svg circle').css('stroke-dashoffset', result);
}); 


// ---------------------------------------------
// Section mapa
// ---------------------------------------------

// --> Route 
p_emp.find('.tc-get-directions').on('click', function() {

  // Remove previous directions
  if (typeof directionsDisplay != 'undefined') {
    directionsDisplay.setMap(null);
  }
  m_marker.setMap(null);

  // Setup display options
  directionsDisplayOptions = {
    map: map,
    // suppressMarkers: true
  }

  // Create directions objects
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer(directionsDisplayOptions);

  // Get start & end
  var start   = $('#desktopRouteAddress').val(),
  end     = $('#routeEndingPoint').val();

  // Setup request
  var request = {
    origin: start,
    destination: end,
    travelMode: 'DRIVING',
    provideRouteAlternatives: false
  };

  // Generate route
  directionsService.route(request, function(result, status) {

    /* check if directions were found */
    if (status == 'OK') {
      // ?= YES

      /* generate route trace */
      directionsDisplay.setDirections(result);

    } else {
      // ?= NO

      console.log('Sem rotas');
    }
  });

});

// --> Fitler category
p_emp.find('.section-mapa .markers li').on('click', function() {

  // ||> Variables
  var _this   = $(this),
  categoria   = $(this).attr('categoria');

  // ||> Set markers visible or no
  m_markers.forEach(function(marker) {
    if (marker.category == categoria) {
      var visibility = marker.getVisible();
      if (visibility) {
        marker.setVisible(false);
        _this.removeClass('active');
      } else {
        marker.setVisible(true);
        _this.addClass('active');
      }
    }
  })
}); 




p_emp.find(".regular").slick({
  dots: true,
  infinite: false,
  slidesToShow: 5,
  slidesToScroll: 1
});


