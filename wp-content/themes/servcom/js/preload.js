
/**
 * Triggers animation for page elements.
 */
function preloader_entrance() {
  let elements = $('[entrance]');

  /** Trigger page. */
  let page_delay = Number($('main.page').attr('delay')) || 0;
  setTimeout(() => $('main.page').trigger('ready').removeAttr('delay'), page_delay);

  /** Enable window events && activate body stylings. */
  setTimeout(() => $(window).trigger('entered'), page_delay);
  setTimeout(() => $('body').addClass('entered'), page_delay);

  /** Loop through elements. */
  elements.each(function() {
    let el = $(this);
    let delay = Number(el.attr('entrance'));
    setTimeout(() => el.addClass('entered').removeAttr('entrance').trigger('entered'), delay);
  })
}

/**
 * Animate preloader, then trigger elements entrances.
 */
(function() {
  let preloader = $('#preloader');

  setTimeout(() => preloader.addClass('show'), 500);

  /** Wait for the page to finish loading. */
  window.onload = function() {

    /** Check for page transition. */
    if (get_url_param('a') === false) {
      setTimeout(() => {

        /** Animate. */
        preloader.prop('progress', 0).animate({
          progress: 100
        },{
          duration: 1500,
          easing: $.bez([0.5, 0, 0, 1]),
          step: val => {
            val = val.toFixed(0);
            preloader.find('.logo-filled').css('clip-path', 'polygon(0 0, '+val+'% 0, '+val+'% 100%, 0 100%)');
          },
          complete: () => {
            setTimeout(() => preloader.addClass('finished'), 500);

            /** Trigger page and elements entrance. */
            setTimeout(() => preloader_entrance(), 2000);
          }
        })
      }, 1500);
    } else {
      setTimeout(() => preloader.addClass('finished'), 500);

      /** Clear URL. */
      var clean_uri = location.protocol + "//" + location.host + location.pathname;
      window.history.replaceState({}, document.title, clean_uri);

      /** Trigger page and elements entrance. */
      setTimeout(() => preloader_entrance(), 2000);
    }
  }
}())
