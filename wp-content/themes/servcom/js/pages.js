
class PageController {

  /** @construct */
  constructor() {

    /** Get this page. */
    this.thePage = $("main.page");
    this.thePageKey = this.thePage.attr("key");
  }

  /**
   * Retrieves a specific section in the current page.
   *
   * @param  {string} key Section key.
   *
   * @return {Object}     Section element, if found.
   */
  section(key) {

    /** Attempt to find the specified section. */
    let section = this.currentPage.find('.section[key="'+key+'"]');
    return (section.length !== 0) ? section : false;
  }
}

/** @inst: Page controller. */
const pageCtrl = new PageController();

// MAIN ACTION
// ----------------------------------------

/**
 * Pages list.
 * @type {Object}
 */
const pages_list = {}

/** Check for page controller. */
if (typeof pages_list[pageCtrl.currentPageKey] !== "undefined") {

  /** Create a new instance for the current page. */
  const Page = new pages_list[pageCtrl.currentPageKey];

  /** @init */
  Page.init();
  console.warn('Page %c'+Page.pageTitle+' %chas initialized.', 'font-weight: bold;', 'font-weight: normal;');
}

const p = {
  contact: $('#page-contact'),
  home : $('#page-home')
}


if (p.contact.length !== 0) {

  /** Get tabs information. */
  let tabs_wrapper = p.contact.find('.section-contact .contact-tabs .tabs');
  let heights = [];
  tabs_wrapper.find('.tab').each(function() {
    heights.push($(this).outerHeight());
  })

  /** Apply the highest value to the parent. */
  let highest = Math.max(...heights);
  tabs_wrapper.css('height', highest);
}

/**
 * @section [Contact]
 *
 * Switch tabs.
 */
p.contact.find('.navigation .item button').on('click', function() {


  /** Get tab index. */
  let item = $(this).parent();
  let index = $(this).attr('index');



  /** Get main wrapper. */
  let wrapper = p.contact.find('.contact-tabs');

  /** Deactivate all tabs. */
  wrapper.find('.tabs .tab').removeClass('active');
  setTimeout(() => wrapper.find(`.tabs .tab[index="${index}"]`).addClass('active'), 700);

  /** Deactivate all items. */
  item.parents('.navigation').find('.item').removeClass('active');
  item.addClass('active');
})




/** @setup: Slide. */

function setup__heroSlide() {


  /** Get main elements. */
  let section = $('.section-hero');
  let nav = section.find(".slide .slide-arrows");

  /** Activate the first navigation item. */
  nav.find('.item[data-index="0"]').addClass("active");

  /** @setInterval */
  window.heroInterval = setInterval(() => hero__changeSlide("next"), 7000);

  /** Bind click event to the arrows. */
  nav.find(".arrow").on("click", function() {
    hero__changeSlide($(this).data("direction"));
  })

  /** Bind click event to the thumbnails. */
  nav.find(".dots .item button").on("click", function() {
    let slideIndex = $(this).parent(".item").data("index");
    hero__changeSlide(slideIndex);
  });
}

if(p.home.length !== 0) setup__heroSlide();

/** @action: Change slide. */
function hero__changeSlide(direction) {

  let isSpecific = !(direction === "next" || direction === "prev");

  /** Get main elements. */
  let section = $('.section-hero');
  let slide = section.find(".slide");

  if (slide.is(".changing")) return;
  slide.addClass("changing");

  /** @clearInterval */
  clearInterval(window.heroInterval);

  /** Get the current slide. */
  let currentSlide = slide.find(".item.active");
  let currentSlideIndex = currentSlide.data("index");

  /** Get the next slide. */
  let nextSlideIndex = (direction === "next") ? currentSlideIndex + 1 : currentSlideIndex - 1;
  if (nextSlideIndex < 0) nextSlideIndex = (slide.find(".slide-itens .item").length - 1);
  if (nextSlideIndex === slide.find(".slide-itens .item").length) nextSlideIndex = 0;
  if (isSpecific) nextSlideIndex = direction;
  let nextSlide = slide.find(`.item[data-index="${nextSlideIndex}"]`);

  /** Change the active slide. */
  currentSlide.removeClass("active");
  setTimeout(() => {
    nextSlide.addClass("active")
    setTimeout(() => slide.removeClass("changing"), 1000);
    window.heroInterval = setInterval(() => hero__changeSlide("next"), 7000);
  }, 1000);
}
