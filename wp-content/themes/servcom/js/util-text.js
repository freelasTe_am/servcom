
/**
 * Shuffles array items.
 *
 * @param  {Array} o Array to shuffle.
 * @return {Array}   Shuffled array.
 */
function shuffle_array(o) {
  for (let j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
}

/**
 * Splits text inside the element.
 *
 * @param  {Object} element           Element to split the text on.
 * @param  {String} [entity='letter'] Entity to split by. Can be [letter], [word] or [line].
 * @return {Array}                    Splitted items.
 */
function split_text(element, entity='letter') {

  /** Declare main variable */
  let split = [];

  if (element.hasClass('splitted')) {

    /** Get splitted items */
    element.find('.line .'+entity).each(function() {
      split.push(this)
    })
  } else {

    /** Split text by lines */
    let split_lines = element.text().trim().split("\n");

    /** Loop through splitted lines */
    element.html('');
    for (let li = 0; li < split_lines.length; li++) {
      let line = split_lines[li].trim();

      /** Split line by words */
      let words = line.split(' ');

      /** Create line element */
      let line_el = document.createElement('div');
      line_el.className = 'line';

      /** Loop through splitted words */
      for (let wi = 0; wi < words.length; wi++) {
        let word = words[wi];

        /** Split word by letters */
        let letters = word.split('');

        /** Create word element */
        let word_el = document.createElement('div');
        word_el.className = 'word';

        /** Loop through letters */
        for (let i = 0; i < letters.length; i++) {
          let letter = letters[i];

          if (letter !== ' ') {

            /** Create letter element */
            let letter_el = document.createElement('div');
            letter_el.className = 'letter';
            letter_el.innerHTML = letter;

            /** Push letter into main array */
            if (entity === 'letter') split.push(letter_el);

            /** Append letter to word */
            $(letter_el).appendTo(word_el);
          }
        }

        /** Push word into main array */
        if (entity === 'word') split.push(word_el);

        /** Append word to line */
        $(word_el).appendTo(line_el);
      }

      /** Append line to element */
      element.append(line_el).addClass('splitted');
    }
  }

  return split;
}

/**
 * Animates entrance for splitted text.
 *
 * @param  {Object}  element      Element to animate.
 * @param  {Object}  options_from Additional options.
 * @param  {Object}  options_to   Additional options.
 * @return {Promise}              Resolved when the animation has finished.
 */
function split_entrance(element, options, callback=false) {

  /** Get random number inside a range */
  function random(min, max) {
    return Math.random() * (max - min) + min;
  }

  /** Setup default options */
  let default_options = {
    wait: 0,
    entity: 'letter',
    shuffle: false,
    transition: 0.5,
    delay: 0.05,
    from: {
      x: [0,0],
      y: [0,0],
      z: [0,0],
      opacity: 0,
      scale: 1
    },
    to: {
      x: [0,0],
      y: [0,0],
      z: [0,0],
      opacity: 1,
      scale: 1
    }
  }

  /** Merge default options */
  for (let prop in default_options) {
    if (typeof options[prop] === 'undefined') options[prop] = default_options[prop];
  }

  /** Split text */
  let split = split_text(element, options.entity);
  setTimeout(function() {

    /** Hide element */
    element.css('visibility', 'visible');

    /** ? Shuffle array */
    if (options.shuffle) split = shuffle_array(split)

    /** Loop through split items */
    $(split).each(function(i, item) {

      /** Setup TweenMax options */
      let tm_options_from = {
        repeat: 0,
        repeatDelay: 10,
        ease: Strong.easeOut,
        opacity: options.from.opacity,
        scale: options.from.scale,
        x: random(options.from.x[0], options.from.x[1]),
        y: random(options.from.y[0], options.from.y[1]),
        z: random(options.from.z[0], options.from.z[1]),
        delay: i * options.delay
      }
      let tm_options_to = {
        repeat: 0,
        repeatDelay: 10,
        ease: Strong.easeOut,
        opacity: options.to.opacity,
        scale: options.to.scale,
        x: random(options.to.x[0], options.to.x[1]),
        y: random(options.to.y[0], options.to.y[1]),
        z: random(options.to.z[0], options.to.z[1]),
        delay: i * options.delay
      }

      /** Animate entrance */
      TweenMax.fromTo($(item), options.transition, tm_options_from, tm_options_to);
    });

    /** Callback */
    let transition_timeout = options.transition * 1000;
    let total_delay = $(split).length * (options.delay * 500);
    let total_timeout = transition_timeout + total_delay;

    setTimeout(function() {
      if (callback) callback(true)
    }, total_timeout);

  }, options.wait)
}
